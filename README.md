# lua_time
A Lua module written in C that adds high precision timestamps and sleeping.

# Variety
Timestamps:
* time(), Seconds (same as os.time)
* mtime(), Milliseconds
* utime(), Microseconds
* ntime(), Nanoseconds

Sleeping:
* sleep(), Seconds
* msleep(), Milliseconds
* usleep(), Microseconds
* nsleep(), Nanoseconds

# How to use?
1. `make`
2. `sudo make install # or cd build`
3. `> local time = require("time")`
See **examples/timer.lua**.

# License
Lua_time is licensed under the GNU GPlv3, available in [COPYING](COPYING).
