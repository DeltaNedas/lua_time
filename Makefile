CC ?= gcc
STRIP := strip

PREFIX ?= /usr
LUA := 5.3

STANDARD := c99
CFLAGS ?= -O3 -Wall -pedantic -g
override CFLAGS += -std=$(STANDARD) -I$(PREFIX)/include/lua$(LUA)
LDFLAGS := -lm

# For installations
LIBRARIES := $(PREFIX)/lib/lua/$(LUA)

LIBRARY := time.so

sources := $(shell find src -type f -name "*.c")
objects := $(sources:src/%.c=build/%.o)
depends := $(sources:src/%.c=build/%.d)

all: $(LIBRARY)

build/%.o: src/%.c
	@printf "CC\t%s\n" $@
	@mkdir -p `dirname $@`
	@$(CC) $(CFLAGS) -c -fPIC -MMD -MP $< -o $@

-include $(depends)

$(LIBRARY): $(objects)
	@printf "CCLD\t%s\n" $@
	@$(CC) $^ -shared -o $@ $(LDFLAGS)

clean:
	rm -rf build

strip: all
	$(STRIP) $(LIBRARY)

install: all
	cp -f $(LIBRARY) $(LIBRARIES)

.PHONY: all clean strip install
