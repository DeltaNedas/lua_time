#define _POSIX_C_SOURCE 200809L
#include <time.h>
#include <inttypes.h>
#include <math.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

unsigned long timestamp() {
	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);

	return spec.tv_sec * 1e9 + spec.tv_nsec;
}

int time_ntime(lua_State* L) {
	lua_pushnumber(L, timestamp());
	return 1;
}
int time_utime(lua_State* L) {
	lua_pushnumber(L, round(timestamp() / 1e3));
	return 1;
}
int time_mtime(lua_State* L) {
	lua_pushnumber(L, round(timestamp() / 1e6));
	return 1;
}
int time_time(lua_State* L) {
	lua_pushnumber(L, round(timestamp() / 1e9));
	return 1;
} // os.time() really


int sleep_nanoseconds(lua_State* L, int place) {
	double nano = luaL_checknumber(L, 1);
	if (nano < 0) return 0;
	nano *= place;

	struct timespec spec;
	spec.tv_sec = nano / 1e9;
	spec.tv_nsec = fmod(nano, 1e9);
	nanosleep(&spec, &spec);
	return 0;
}

#define SLEEP(degree, mul) \
	int time_##degree(lua_State* L) { \
		return sleep_nanoseconds(L, mul); \
	}

SLEEP(nsleep, 1)
SLEEP(usleep, 1e3)
SLEEP(msleep, 1e6)
SLEEP(sleep, 1e9)

static struct luaL_Reg time_lib[] = {
	{"ntime", time_ntime},
	{"utime", time_utime},
	{"mtime", time_mtime},
	{"time", time_time}, // os.time()
	{"nsleep", time_nsleep},
	{"usleep", time_usleep},
	{"msleep", time_msleep},
	{"sleep", time_sleep},
	{NULL, NULL} // sentinel
};

int luaopen_time(lua_State* L) {
	luaL_newlib(L, time_lib);
	return 1;
}
